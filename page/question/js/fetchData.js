const query = `
query {
    questions {
      id
      code
      content
    }
  }
`,
  url = "https://survey-untirta-backend.ms-prakarsa.id/graphql",
  datas= [],
  opts = {
  method: "POST",
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  },
body: JSON.stringify({ query })
};
fetch(url, opts)
  .then(r => r.json())
  .then(({ data }) => (document.getElementById('id_3').innerHTML = data.questions
    .map(r => `
      <label class="form-label form-label-top form-label-auto" id="${r.id}" for="${r.id}"> ${r.content}  </label>
      <form id=${r.id} class="form-input-wide">
        <fieldset class="selections" id="group-${r.code}" style="border: none; margin-bottom: 1rem;margin-top: 2rem;">
          <label class="container-radio">Sangat Tidak Baik
            <input type="radio" name="radio" value="sangatTidakBaik" name="group-${r.code}">
            <span class="checkmark opt-1" name="group-${r.code}"></span>
          </label>
          <label class="container-radio">Tidak Baik
            <input type="radio" name="radio" value="tidakBaik" name="group-${r.code}">
            <span class="checkmark opt-2" name="group-${r.code}"></span>
          </label>
          <label class="container-radio">Cukup
            <input type="radio" name="radio" value="Cukup" name="group-${r.code}">
            <span class="checkmark opt-3" name="group-${r.code}"></span>
          </label>
          <label class="container-radio opt-3">Baik
            <input type="radio" name="radio" value="Baik" name="group-${r.code}">
            <span class="checkmark opt" name="group-${r.code}"></span>
          </label>
          <label class="container-radio">Sangat Baik
            <input type="radio" name="radio" value="sangatBaik" name="group-${r.code}">
            <span class="checkmark opt-4" name="group-${r.code}"></span>
          </label>
        </fieldset>
      </form>
    `)
    .join()
  ))
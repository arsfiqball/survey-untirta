$(document).ready(function(){
    $('.roleSurviyor').trigger('change'); //This event will fire the change event. 
    $('.roleSurviyor').change(function(){
      var data= $(this).val();
      console.log('data',data)
      if (data == 'Mahasiswa') {
          $('.role-mahasiswa').css('display','block')
          $('.role-dosen').css('display','none')
          $('.role-masyarakat').css('display','none')
      } else if (data == 'Dosen') {
        $('.role-mahasiswa').css('display','none')
        $('.role-dosen').css('display','block')
        $('.role-masyarakat').css('display','none')        
      } else if (data =='Masyarakat') {
        $('.role-mahasiswa').css('display','none')
        $('.role-dosen').css('display','none')
        $('.role-masyarakat').css('display','block')                  
      }
    });

    $('.btn').on('click',function(e) {
        e.preventDefault();
        var $this = $('#DynamicValueAssignedHere'),
        _selectRole = $this.find('select[name="class"]').val(),
        _name = $this.find('input[name="name"]').val(),
        _nim = $this.find('input[name="nim"]').val(),
        _gender = $this.find('select[name="gender"]').val(),
        _fakultas = $this.find('input[name="fakultas"]').val(),
        _no_Telp = $this.find('input[name="no_Telp"]').val(),
        data = {
            selectRole: _selectRole,
            name: _name,
            nim: _nim,
            gender: _gender,
            fakultas: _fakultas,
            no_Telp: _no_Telp,
        };
        console.log('ini submit _selectRole',data);
        if ( (_name && _nim  && _fakultas !== '') && (_gender !== null)) {
            document.location.href='./page/question/Website_Survey.html';
            localStorage.setItem('data_Personal', JSON.stringify(data))
        } else {
            alert('kosong bos')
        }
    });
  });